setup:
	docker swarm init

up-0-portainer:
	docker-compose -f ./0.portainer/docker-compose.yaml up -d  portainer

up-1-nginx-manager:
	docker stack deploy -c ./1.nginx/docker-compose.yaml nginx-manager

up-2-keycloak:
	docker stack deploy -c ./2.keycloak/docker-compose.yaml keycloak

up-3-oracle:
	docker stack deploy -c ./3.oracle/docker-compose.yaml oracle

down-0-portainer:
	docker-compose -f ./0.portainer/docker-compose.yaml down

down-1-nginx-manager:
	docker stack rm nginx-manager

down-2-keycloak:
	docker stack rm keycloak

down-3-oracle:
	docker stack rm oracle